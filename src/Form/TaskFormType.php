<?php


namespace App\Form;
use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class TaskFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add( 'task', TextType::class, [
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Task name'
                ),
                'constraints' => [
                    new NotBlank(['message' => 'Please enter task name.'])
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => false,

                'attr' => array(
                    'rows' => '5',
                    'placeholder' => 'Description'
                )
            ])
            ->add('contractor', EntityType::class, [
                'label' => false,
                'placeholder' => 'Assigned to',
                'class' => User::class,
                'constraints' => [
                    new NotBlank(['message' => 'Please choose member.'])
                ]
            ])
            ->add('priority', ChoiceType::class, [
                'label' => false,
                'placeholder' => 'Priority',
                'constraints' => [
                    new NotBlank(['message' => 'Please choose priority.'])
                ],
                'choices'  => [
                    'Low' => 1,
                    'Medium' => 2,
                    'High' => 3
                ]
            ])
            ->add('status', ChoiceType::class, [
                'label' => false,
                'placeholder' => 'Status',
                'constraints' => [
                    new NotBlank(['message' => 'Please enter workspace name.'])
                ],
                'choices'  => [
                    'Done' => 1,
                    'In progress' => 2,
                    'Waiting' => 3
                ]
            ])
            ->add('dueDate', DateType::class, [
                'widget' => 'single_text',
                'constraints' => [
                     new NotBlank(['message' => 'Please enter workspace name.'])
                ]
            ])
            ->add('addTask' , SubmitType::class);
    }
}
