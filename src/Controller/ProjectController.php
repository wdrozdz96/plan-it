<?php


namespace App\Controller;

use App\Entity\Domain;
use App\Entity\Project;
use App\Entity\Task;
use App\Entity\User;
use App\Form\ProjectFormType;
use App\Form\addUsersFormType;
use App\Form\TaskFormType;
use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController extends AbstractController
{
    private $session;


    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/main/projects", name="app_addUsers")
     */
    public function new(Request $request): Response
    {
        if ($this->isGranted("IS_AUTHENTICATED_FULLY")) {
            // -- setting up basic data ---
            $currentWorkspace = $this->session->get('currentWorkspace');
            $currentProject = $this->session->get('currentProject');
            $currentUser = $this->getUser()->getID();

            $domain = $this->getDoctrine()->getRepository(Domain::class)->find($currentWorkspace);
            if ($currentProject) {
                $projectObject = $this->getDoctrine()->getRepository(Project::class)->find($currentProject);
            }

            $project = new Project();
            $user = new Domain();
            $task = new Task();

            $taskForm = $this->createForm(TaskFormType::class, $task);
            $userForm = $this->createForm(addUsersFormType::Class, $user);
            $projectForm = $this->createForm(ProjectFormType::Class, $project);

            $taskForm->handleRequest($request);
            $userForm->handleRequest($request);
            $projectForm->handleRequest($request);

            if ($userForm->isSubmitted() && $userForm->isValid()) {
                $users = $userForm->getData('users');
                $users = $users->getUsers();
                $usersObjectsArray = $this->getDoctrine()->getRepository(User::class)->findByEmail($users);

                $domain->addDomainUser(reset($usersObjectsArray));
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($domain);
                $entityManager->flush();
                return $this->redirect("/main/projects");
            } elseif ($projectForm->isSubmitted() && $projectForm->isValid()) {
                $project->setDomain($domain);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($project);
                $entityManager->flush();
                return $this->redirect("/main/projects");
            } elseif ($taskForm->isSubmitted() && $taskForm->isValid()) {
                $time = date('Y-m-d');
                $task->setStartDate(DateTime::createFromFormat('Y-m-d', $time));
                $task->setCreator($currentUser);
                $projectObject->addTask($task);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($projectObject);
                $entityManager->flush();
                return $this->redirect("/main/projects");
            }

            return $this->render('projects/project_form.html.twig', ['user_form' => $userForm->createView(), 'project_form' => $projectForm->createView(), 'task_form' => $taskForm->createView()]);

        }
        return $this->redirectToRoute("app_welcome");
    }

    /**
     * @Route("/main/projects", name="app_displayProjects")
     */
    public function showProjects()
    {
        if ($this->isGranted("IS_AUTHENTICATED_FULLY")) {
            $currentWorkspace = $this->session->get('currentWorkspace');
            $currentProject = $this->session->get('currentProject');

            $projects = $this->getDoctrine()
                ->getRepository(Project::class)
                ->findByWorkspace($currentWorkspace);

            $tasks = $this->getDoctrine()
                ->getRepository(Task::class)
                ->findByProject($currentProject);

            return $this->render('/projects/show_projects.html.twig', array('projects' => $projects, 'tasks' => $tasks));
        } else {
            return $this->redirectToRoute("app_main");
        }
    }

    /**
     * @Route("/main/projects", name="app_displayTasks")
     */
    public function showTasks()
    {
        if ($this->isGranted("IS_AUTHENTICATED_FULLY")) {
            $currentWorkspace = $this->session->get('currentWorkspace');
            $currentProject = $this->session->get('currentProject');


            $tasks = $this->getDoctrine()
                ->getRepository(Task::class)
                ->findByProject($currentProject);

            return $this->render('/projects/show_tasks.html.twig', array('tasks' => $tasks));
        } else {
            return $this->redirectToRoute("app_main");
        }
    }


    /**
     * @Route("projects/select/{currentProject}/{currentProjectName}")
     */
    public function select($currentProject, $currentProjectName)
    {


        $this->session->set('currentProject', $currentProject);
        $this->session->set('currentProjectName', $currentProjectName);
        return $this->redirectToRoute("app_displayProjects");
    }

    /**
     * @Route("main/projects/deleteTask/{id}")
     */

    public function deleteTask($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $task = $this->getDoctrine()->getRepository(Task::class)->find($id);

        $entityManager->remove($task);
        $entityManager->flush();
        return $this->redirect("/main/projects");

    }

    /**
     * @Route("main/projects/deleteProject/{id}")
     */

    public function deleteProject($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $project = $this->getDoctrine()->getRepository(Project::class)->find($id);

        $entityManager->remove($project);
        $entityManager->flush();
        $this->session->set('currentProject', '');
        return $this->redirect("/main/projects");

    }

}

