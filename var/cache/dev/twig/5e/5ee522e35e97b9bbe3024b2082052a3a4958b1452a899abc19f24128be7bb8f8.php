<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* workspaces/workspaces.html.twig */
class __TwigTemplate_f194aa999f4abdb8d7eb214aeb53a706b8849b76143e28b5298768e1e1f1363a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "workspaces/workspaces.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "workspaces/workspaces.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "workspaces/workspaces.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Choose workspace";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <style>
        body {
            background-color: #f5f5f5;
        }

        .workspaces-wrapper {
            margin: 60px;
            margin-top: 80px;

        }

        .workspace-table {
            box-shadow: 0 0 2px #a5a3a3;

        }

        .th-style {
            background-color: #eaeaea !important;

        }

        .btn {
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
        }

        .table th {
            background: #2196F3 -webkit-gradient(linear, left top, left bottom, from(#42a6f5), to(#2196F3)) repeat-x;
            justify-content: center;
            align-items: center;
            color: white;
            border-top-width: 0px;
        }
    </style>
    <h1 class=\"text-center mt-5\">Welcome ";
        // line 37
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 37, $this->source); })()), "User", [], "any", false, false, false, 37), "name", [], "any", false, false, false, 37), "html", null, true);
        echo "!</h1>

    ";
        // line 39
        if ((isset($context["workspaces"]) || array_key_exists("workspaces", $context) ? $context["workspaces"] : (function () { throw new RuntimeError('Variable "workspaces" does not exist.', 39, $this->source); })())) {
            // line 40
            echo "        <h4 class=\"text-center mt-2 mb-3\">Choose your workspace.</h4>
        <div class=\" workspaces-wrapper\">
            <table id=\"workspace\" class=\"table table-light workspace-table\">
                <thead class=\"th-style\">
                <tr>
                    <th scope=\"col\">Workspace name</th>
                    <th scope=\"col\">Actions</th>
                </tr>
                </thead>

                ";
            // line 50
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["workspaces"]) || array_key_exists("workspaces", $context) ? $context["workspaces"] : (function () { throw new RuntimeError('Variable "workspaces" does not exist.', 50, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["domain"]) {
                // line 51
                echo "                    <!-- table row -->
                    <tr>
                        <td>
                            ";
                // line 54
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["domain"], "domain", [], "any", false, false, false, 54), "html", null, true);
                echo "
                        </td>
                        <td>
                            <!-- select workspace-->
                            <a href=\"/main/select/";
                // line 58
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["domain"], "id", [], "any", false, false, false, 58), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["domain"], "domain", [], "any", false, false, false, 58), "html", null, true);
                echo "\" class=\"btn btn-outline-primary\">Select</a>
                            <!-- remove workspace modal trigger -->
                            <button type=\"button\" class=\"btn btn-outline-danger\" data-toggle=\"modal\"
                                    data-target=\"#removeModal-";
                // line 61
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["domain"], "id", [], "any", false, false, false, 61), "html", null, true);
                echo "\">Delete
                            </button>
                        </td>
                    </tr>

                    <!-- remove-modal -->
                    <div class=\"modal fade\" id=\"removeModal-";
                // line 67
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["domain"], "id", [], "any", false, false, false, 67), "html", null, true);
                echo "\" tabindex=\"-1\" role=\"dialog\"
                         aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
                        <div class=\"modal-dialog\" role=\"document\">
                            <div class=\"modal-content\">
                                <div class=\"modal-header\">
                                    <h5 class=\"modal-title\" id=\"exampleModalLabel\">Are you sure?</h5>
                                </div>
                                <div class=\"modal-body\">
                                    <a class=\"blockquote mt-3 mb-3\">This action will remove workspace and all projects
                                        that
                                        it contains</a>
                                </div>
                                <div class=\"modal-footer\">
                                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                                    <a href=\"/main/delete/";
                // line 81
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["domain"], "id", [], "any", false, false, false, 81), "html", null, true);
                echo "\"
                                       class=\"btn btn-danger delete-workspace\">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>

                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['domain'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 89
            echo "            </table>
        </div>
    ";
        } elseif ( !        // line 91
(isset($context["workspaces"]) || array_key_exists("workspaces", $context) ? $context["workspaces"] : (function () { throw new RuntimeError('Variable "workspaces" does not exist.', 91, $this->source); })())) {
            // line 92
            echo "        <div class=\"text-center\">
            <h5>You dont have any workspaces.</h5>
            <!-- add workspace modal trigger -->
        </div>
    ";
        }
        // line 97
        echo "    <div class=\"text-center\">


    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "workspaces/workspaces.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 97,  212 => 92,  210 => 91,  206 => 89,  192 => 81,  175 => 67,  166 => 61,  158 => 58,  151 => 54,  146 => 51,  142 => 50,  130 => 40,  128 => 39,  123 => 37,  88 => 4,  78 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% block title %}Choose workspace{% endblock %}
{% block body %}
    <style>
        body {
            background-color: #f5f5f5;
        }

        .workspaces-wrapper {
            margin: 60px;
            margin-top: 80px;

        }

        .workspace-table {
            box-shadow: 0 0 2px #a5a3a3;

        }

        .th-style {
            background-color: #eaeaea !important;

        }

        .btn {
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.4);
        }

        .table th {
            background: #2196F3 -webkit-gradient(linear, left top, left bottom, from(#42a6f5), to(#2196F3)) repeat-x;
            justify-content: center;
            align-items: center;
            color: white;
            border-top-width: 0px;
        }
    </style>
    <h1 class=\"text-center mt-5\">Welcome {{ app.User.name }}!</h1>

    {% if workspaces %}
        <h4 class=\"text-center mt-2 mb-3\">Choose your workspace.</h4>
        <div class=\" workspaces-wrapper\">
            <table id=\"workspace\" class=\"table table-light workspace-table\">
                <thead class=\"th-style\">
                <tr>
                    <th scope=\"col\">Workspace name</th>
                    <th scope=\"col\">Actions</th>
                </tr>
                </thead>

                {% for domain in workspaces %}
                    <!-- table row -->
                    <tr>
                        <td>
                            {{ domain.domain }}
                        </td>
                        <td>
                            <!-- select workspace-->
                            <a href=\"/main/select/{{ domain.id }}/{{ domain.domain }}\" class=\"btn btn-outline-primary\">Select</a>
                            <!-- remove workspace modal trigger -->
                            <button type=\"button\" class=\"btn btn-outline-danger\" data-toggle=\"modal\"
                                    data-target=\"#removeModal-{{ domain.id }}\">Delete
                            </button>
                        </td>
                    </tr>

                    <!-- remove-modal -->
                    <div class=\"modal fade\" id=\"removeModal-{{ domain.id }}\" tabindex=\"-1\" role=\"dialog\"
                         aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
                        <div class=\"modal-dialog\" role=\"document\">
                            <div class=\"modal-content\">
                                <div class=\"modal-header\">
                                    <h5 class=\"modal-title\" id=\"exampleModalLabel\">Are you sure?</h5>
                                </div>
                                <div class=\"modal-body\">
                                    <a class=\"blockquote mt-3 mb-3\">This action will remove workspace and all projects
                                        that
                                        it contains</a>
                                </div>
                                <div class=\"modal-footer\">
                                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                                    <a href=\"/main/delete/{{ domain.id }}\"
                                       class=\"btn btn-danger delete-workspace\">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>

                {% endfor %}
            </table>
        </div>
    {% elseif not workspaces %}
        <div class=\"text-center\">
            <h5>You dont have any workspaces.</h5>
            <!-- add workspace modal trigger -->
        </div>
    {% endif %}
    <div class=\"text-center\">


    </div>
{% endblock %}
", "workspaces/workspaces.html.twig", "/home/dev/mps/templates/workspaces/workspaces.html.twig");
    }
}
