<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* landing_page/landing_page.html.twig */
class __TwigTemplate_01cbdac9f6480370630d870d2723e23715d19b3954c9639f94a8e113856658e0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "landing_page/landing_page.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "landing_page/landing_page.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "landing_page/landing_page.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Register";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    <meta name=\"google-signin-client_id\" content=\"YOUR_CLIENT_ID.apps.googleusercontent.com\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "    <style xmlns=\"http://www.w3.org/1999/html\">
        .btn-show-password {
            height: 45px;
        }

        .register-form-wrapper {
            width: 100%;
            height: 100vh;
            justify-content: flex-end;
            margin: 0;
        }

        .register-form {
            height: 100%;
            width: 23%;
        }

        body {
            background: linear-gradient(to bottom, rgba(0, 0, 0, .58) 0, rgba(0, 0, 0, .6) 100%), url(\"https://images.unsplash.com/photo-1484480974693-6ca0a78fb36b\");
            background-size: cover;
            background-repeat: no-repeat;
        }

        .abcRioButton {
            width: 100% !important;
        }

        .big-logo-wrapper {
            display: flex;
            width: 100%;
            justify-content: center;
        }

        .big-logo {
            height: 9vh;
            width: 9vh;
        }

        .welcome-message-wrapper {
            position: absolute;
            top: 23vh;
            left: 16vh;

        }

        .welcome-message {
            color: #f2c194 !important;
        }

        .welcome-message-big {
            font-size: 10vh
        }

        .welcome-message-small {
            font-size: 2.3vh
        }

        .welcome-message-list {
            font-size: 2vh
        }

        .sign-in {
            color: #e3832b !important;
            font-size: 2.4vh;
        }
    </style>

    <div class=\"welcome-message-wrapper\">
        <p class=\"text-primary welcome-message welcome-message-big\">PlanIt.</p>
        <p class=\"text-primary welcome-message welcome-message-small\">Use PlanIt as tool to plan your projects, work,
            school and anything you want for free!</p>
        <p class=\"text-primary welcome-message welcome-message-small\">More about PlanIt:</p>
        <ul class=\"welcome-message welcome-message-list\">
            <li> Work on your projects with group</li>
            <li> Menage and use built in chat</li>
            <li> You can get notifications via mail when someone assign task to you,<br> or when someone complete task
                assigned by you
            </li>
            <li> Share your plans and works to: client, teacher or anyone else with only few clicks</li>
        </ul>
    </div>

    <div class=\"row register-form-wrapper\">
        <div class=\"card bg-light register-form\">

            <div class=\"card-body\">
                <div class=\"big-logo-wrapper\">
                    <img class=\"big-logo mb-4\"
                         src=\"https://www.freelogodesign.org/file/app/client/thumb/54307ce0-ac13-4793-8e4b-24f43579daa5_200x200.png?1574947640270\">
                </div>
                <h1 class=\"text-left mt-5\">Sign up</h1>
                <p class=\"mb-4\">Create your free account.</p>
                ";
        // line 99
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 99, $this->source); })()), 'form_start');
        echo "
                ";
        // line 100
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 100, $this->source); })()), "name", [], "any", false, false, false, 100), 'row');
        echo "
                ";
        // line 101
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 101, $this->source); })()), "email", [], "any", false, false, false, 101), 'row');
        echo "
                ";
        // line 102
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 102, $this->source); })()), "plainPassword", [], "any", false, false, false, 102), 'row');
        echo "
                ";
        // line 103
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 103, $this->source); })()), "agreeTerms", [], "any", false, false, false, 103), 'row');
        echo "
                <button class=\"btn btn-primary btn-block mb-2\">Register</button>
                ";
        // line 105
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 105, $this->source); })()), 'form_end');
        echo "
                <div id=\"my-signin2\"></div>
                <div class=\"text-center mt-4\">
                    Already have account?<br>
                    <a class=\"sign-in mt-2\" href=\"/sign-in\">Sign in.</a>
                </div>
            </div>
        </div>
    </div>

    <script>
        function onSuccess(googleUser) {
            console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
        }

        function onFailure(error) {
            console.log(error);
        }

        function renderButton() {
            gapi.signin2.render('my-signin2', {
                'scope': 'profile email',
                'width': 240,
                'height': 50,
                'longtitle': true,
                'theme': 'dark',
                'onsuccess': onSuccess,
                'onfailure': onFailure
            });
        }
    </script>

    <script src=\"https://apis.google.com/js/platform.js?onload=renderButton\" async defer></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "landing_page/landing_page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 105,  220 => 103,  216 => 102,  212 => 101,  208 => 100,  204 => 99,  110 => 7,  100 => 6,  89 => 4,  79 => 3,  60 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% block title %}Register{% endblock %}
{% block stylesheets %}
    <meta name=\"google-signin-client_id\" content=\"YOUR_CLIENT_ID.apps.googleusercontent.com\">
{% endblock %}
{% block body %}
    <style xmlns=\"http://www.w3.org/1999/html\">
        .btn-show-password {
            height: 45px;
        }

        .register-form-wrapper {
            width: 100%;
            height: 100vh;
            justify-content: flex-end;
            margin: 0;
        }

        .register-form {
            height: 100%;
            width: 23%;
        }

        body {
            background: linear-gradient(to bottom, rgba(0, 0, 0, .58) 0, rgba(0, 0, 0, .6) 100%), url(\"https://images.unsplash.com/photo-1484480974693-6ca0a78fb36b\");
            background-size: cover;
            background-repeat: no-repeat;
        }

        .abcRioButton {
            width: 100% !important;
        }

        .big-logo-wrapper {
            display: flex;
            width: 100%;
            justify-content: center;
        }

        .big-logo {
            height: 9vh;
            width: 9vh;
        }

        .welcome-message-wrapper {
            position: absolute;
            top: 23vh;
            left: 16vh;

        }

        .welcome-message {
            color: #f2c194 !important;
        }

        .welcome-message-big {
            font-size: 10vh
        }

        .welcome-message-small {
            font-size: 2.3vh
        }

        .welcome-message-list {
            font-size: 2vh
        }

        .sign-in {
            color: #e3832b !important;
            font-size: 2.4vh;
        }
    </style>

    <div class=\"welcome-message-wrapper\">
        <p class=\"text-primary welcome-message welcome-message-big\">PlanIt.</p>
        <p class=\"text-primary welcome-message welcome-message-small\">Use PlanIt as tool to plan your projects, work,
            school and anything you want for free!</p>
        <p class=\"text-primary welcome-message welcome-message-small\">More about PlanIt:</p>
        <ul class=\"welcome-message welcome-message-list\">
            <li> Work on your projects with group</li>
            <li> Menage and use built in chat</li>
            <li> You can get notifications via mail when someone assign task to you,<br> or when someone complete task
                assigned by you
            </li>
            <li> Share your plans and works to: client, teacher or anyone else with only few clicks</li>
        </ul>
    </div>

    <div class=\"row register-form-wrapper\">
        <div class=\"card bg-light register-form\">

            <div class=\"card-body\">
                <div class=\"big-logo-wrapper\">
                    <img class=\"big-logo mb-4\"
                         src=\"https://www.freelogodesign.org/file/app/client/thumb/54307ce0-ac13-4793-8e4b-24f43579daa5_200x200.png?1574947640270\">
                </div>
                <h1 class=\"text-left mt-5\">Sign up</h1>
                <p class=\"mb-4\">Create your free account.</p>
                {{ form_start(registrationForm) }}
                {{ form_row(registrationForm.name) }}
                {{ form_row(registrationForm.email) }}
                {{ form_row(registrationForm.plainPassword) }}
                {{ form_row(registrationForm.agreeTerms) }}
                <button class=\"btn btn-primary btn-block mb-2\">Register</button>
                {{ form_end(registrationForm) }}
                <div id=\"my-signin2\"></div>
                <div class=\"text-center mt-4\">
                    Already have account?<br>
                    <a class=\"sign-in mt-2\" href=\"/sign-in\">Sign in.</a>
                </div>
            </div>
        </div>
    </div>

    <script>
        function onSuccess(googleUser) {
            console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
        }

        function onFailure(error) {
            console.log(error);
        }

        function renderButton() {
            gapi.signin2.render('my-signin2', {
                'scope': 'profile email',
                'width': 240,
                'height': 50,
                'longtitle': true,
                'theme': 'dark',
                'onsuccess': onSuccess,
                'onfailure': onFailure
            });
        }
    </script>

    <script src=\"https://apis.google.com/js/platform.js?onload=renderButton\" async defer></script>
{% endblock %}", "landing_page/landing_page.html.twig", "/home/dev/mps/templates/landing_page/landing_page.html.twig");
    }
}
