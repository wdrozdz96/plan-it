<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/connect/google' => [[['_route' => 'connect_google_start', '_controller' => 'App\\Controller\\GoogleController::connectAction'], null, null, null, false, false, null]],
        '/connect/google/check' => [[['_route' => 'connect_google_check', '_controller' => 'App\\Controller\\GoogleController::connectCheckAction'], null, null, null, false, false, null]],
        '/main/projects' => [
            [['_route' => 'app_addUsers', '_controller' => 'App\\Controller\\ProjectController::new'], null, null, null, false, false, null],
            [['_route' => 'app_displayProjects', '_controller' => 'App\\Controller\\ProjectController::showProjects'], null, null, null, false, false, null],
            [['_route' => 'app_displayTasks', '_controller' => 'App\\Controller\\ProjectController::showTasks'], null, null, null, false, false, null],
            [['_route' => 'projects', '_controller' => 'App\\Controller\\ProjectController::displayProjects'], null, null, null, false, false, null],
        ],
        '/welcome' => [[['_route' => 'app_welcome', '_controller' => 'App\\Controller\\RegistrationController::register'], null, null, null, false, false, null]],
        '/sign-in' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/main' => [
            [['_route' => 'app_addWorkspace', '_controller' => 'App\\Controller\\WorkspaceController::new'], null, null, null, false, false, null],
            [['_route' => 'app_main', '_controller' => 'App\\Controller\\WorkspaceController::showAction'], null, null, null, false, false, null],
        ],
        '/' => [[['_route' => 'index', '_controller' => 'App\\Controller\\WorkspaceController::new'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/projects/select/([^/]++)/([^/]++)(*:203)'
                .'|/main/(?'
                    .'|projects/delete(?'
                        .'|Task/([^/]++)(*:251)'
                        .'|Project/([^/]++)(*:275)'
                    .')'
                    .'|delete/([^/]++)(*:299)'
                    .'|select/([^/]++)/([^/]++)(*:331)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception::showAction'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception::cssAction'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        203 => [[['_route' => 'app_project_select', '_controller' => 'App\\Controller\\ProjectController::select'], ['currentProject', 'currentProjectName'], null, null, false, true, null]],
        251 => [[['_route' => 'app_project_deletetask', '_controller' => 'App\\Controller\\ProjectController::deleteTask'], ['id'], null, null, false, true, null]],
        275 => [[['_route' => 'app_project_deleteproject', '_controller' => 'App\\Controller\\ProjectController::deleteProject'], ['id'], null, null, false, true, null]],
        299 => [[['_route' => 'app_workspace_delete', '_controller' => 'App\\Controller\\WorkspaceController::delete'], ['id'], null, null, false, true, null]],
        331 => [
            [['_route' => 'app_workspace_select', '_controller' => 'App\\Controller\\WorkspaceController::select'], ['currentWorkspace', 'currentWorkspaceName'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
