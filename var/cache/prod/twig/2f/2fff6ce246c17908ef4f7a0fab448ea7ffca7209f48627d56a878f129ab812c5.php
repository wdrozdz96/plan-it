<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* projects/show_tasks.html.twig */
class __TwigTemplate_e793016e0800b36322b50a25269abebe388f919f14275ba7903035b2299ce4f0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "projects/show_tasks.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "
    <style>

        .priority-pointer-green {
            font-size: 10px;
            color: #00a6de;
        }

        .priority-pointer-yellow {
            font-size: 10px;
            color: #9500ec;
        }

        .col {
            border-top: 0px;
        }

        .priority-pointer-red {
            font-size: 10px;
            color: red;
        }

        th {
            color: white;
            font-size: 14px;

        }

        td:nth-child(even) {
            background-color: white;
        }

        td:nth-child(odd) {
            background-color: white;
        }

        .card-header {
            background-color: whitesmoke;
        }

        .card-body {
            padding: 0px;
            justify-content: center;

        }

        .card {
            box-shadow: none;

        }

        .table th {
            border-top-width: 0px;
        }

        .table {
            box-shadow: 0 0 5px #7a7878;
            border-radius: 6px;
            background: #2196F3 -webkit-gradient(linear, left top, left bottom, from(#42a6f5), to(#2196F3)) repeat-x;
            width: 99%;
            justify-content: center;
            align-items: center;

        }

        .table-secondary {
            border-top: 0px;
        }

        .table-task {
            margin: 5px;
            background-color: whitesmoke;

        }

        .content-wrapper {
            background-color: whitesmoke;
        }

        .projectheader {
            background-color: white;
        }

        .collapse {
            background-color: whitesmoke;
        }

        .table-active {
            background-color: whitesmoke;
        }
    </style>

    <!--Accordion wrapper-->
    <div class=\"accordion md-accordion\" id=\"accordionEx\" role=\"tablist\" aria-multiselectable=\"false\">

        <!-- Accordion card -->
        <div class=\"card\">

            <!-- Card header -->
            <div class=\"card-header\" role=\"tab\" id=\"done\">
                <a data-toggle=\"collapse\" data-parent=\"#accordionEx\" href=\"#collapseDone\" aria-expanded=\"true\"
                   aria-controls=\"collapseDone\">
                    <h5 class=\"mb-0\" style=\"font-size: 20px;color:rgb(68, 68, 68);\">Done</h5>
                </a>
            </div>

            <!-- Card body -->
            <div id=\"collapseDone\" class=\"collapse show\" role=\"tabpanel\" aria-labelledby=\"done\"
                 data-parent=\"#accordionEx\">
                <div class=\"card-body\">
                    <table id=\"done\" class=\"table table-secondary workspace-table\">
                        <thead>
                        <tr>
                            <th scope=\"col\">Task</th>
                            <th scope=\"col\">Contractor</th>
                            <th scope=\"col\">Priority</th>
                            <th scope=\"col\">Due date</th>
                            <th scope=\"col\" class=\"text-right\">Options</th>
                        </tr>
                        </thead>
                        <tbody class=\"table-task\">
                        ";
        // line 126
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["tasks"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["task"]) {
            // line 127
            echo "                            <tr class=\"table-active\">
                                ";
            // line 128
            if ((twig_get_attribute($this->env, $this->source, $context["task"], "status", [], "any", false, false, false, 128) == 1)) {
                // line 129
                echo "                                    <td class=\"text-left\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "task", [], "any", false, false, false, 129), "html", null, true);
                echo "</td>
                                    <td class=\"text-left\"> ";
                // line 130
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "contractor", [], "any", false, false, false, 130), "html", null, true);
                echo "</td>
                                    ";
                // line 131
                if ((twig_get_attribute($this->env, $this->source, $context["task"], "priority", [], "any", false, false, false, 131) == 1)) {
                    // line 132
                    echo "                                        <td class=\"pl-3\"><p>Low <i class=\"material-icons priority-pointer-green\">brightness_1</i>
                                            </p></td>
                                    ";
                } elseif ((twig_get_attribute($this->env, $this->source,                 // line 134
$context["task"], "priority", [], "any", false, false, false, 134) == 2)) {
                    // line 135
                    echo "                                        <td class=\"pl-3\"><p>Medium <i class=\"material-icons priority-pointer-yellow\">brightness_1</i>
                                            </p></td>
                                    ";
                } elseif ((twig_get_attribute($this->env, $this->source,                 // line 137
$context["task"], "priority", [], "any", false, false, false, 137) == 3)) {
                    // line 138
                    echo "                                        <td class=\"pl-3\"><p>High <i class=\"material-icons priority-pointer-red\">brightness_1</i>
                                            </p>
                                        </td>

                                    ";
                }
                // line 143
                echo "                                    <td class=\"text-left\">";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "dueDate", [], "any", false, false, false, 143), "d-M-y"), "html", null, true);
                echo "
                                    </td>
                                    <td>
                                        <ul class=\"nav nav-pills float-right\">
                                            <li class=\"nav-item dropup dropleft\">
                                                <a class=\"nav-link material-icon\" style=\"padding:0px;,height:3px;\"
                                                   data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\"
                                                   aria-expanded=\"false\"><i class=\"material-icons \"
                                                                            style=\"color:black;\">more_horiz</i></a>
                                                <div class=\"dropdown-menu\" style=\"\">
                                                    <a class=\"dropdown-item\"
                                                       href=\"/main/projects/deleteTask/";
                // line 154
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "id", [], "any", false, false, false, 154), "html", null, true);
                echo "\">Delete</a>
                                                    <a class=\"dropdown-item\"
                                                       href=\"/main/projects/deleteTask/";
                // line 156
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "id", [], "any", false, false, false, 156), "html", null, true);
                echo "\">Update</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                ";
            }
            // line 162
            echo "                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['task'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 164
        echo "                        </tbody>


                    </table>
                </div>
            </div>

        </div>
        <!-- Accordion card -->

        <!-- Accordion card -->
        <div class=\"card\">

            <!-- Card header -->
            <div class=\"card-header\" role=\"tab\" id=\"inProgress\">
                <a data-toggle=\"collapse\" data-parent=\"#accordionEx\" href=\"#collapseInProgress\" aria-expanded=\"false\"
                   aria-controls=\"collapseInProgress\">
                    <h5 class=\"mb-0\" style=\"font-size: 20px;color:rgb(68, 68, 68);\"> In progress</h5>
                </a>
            </div>

            <!-- Card body -->
            <div id=\"collapseInProgress\" class=\"collapse show\" role=\"tabpanel\" aria-labelledby=\"headingInProgress\"
                 data-parent=\"#accordionEx\">
                <div class=\"card-body\">
                    <table id=\"InProgress\" class=\"table table-secondary workspace-table\">
                        <thead>
                        <tr>
                            <th scope=\"col\">Task</th>
                            <th scope=\"col\">Contractor</th>
                            <th scope=\"col\">Priority</th>
                            <th scope=\"col\">Due date</th>
                            <th scope=\"col\" class=\"text-right\">Options</th>
                        </tr>
                        </thead>
                        <tbody class=\"table-task\">
                        ";
        // line 200
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["tasks"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["task"]) {
            // line 201
            echo "                            <tr class=\"table-active\">
                                ";
            // line 202
            if ((twig_get_attribute($this->env, $this->source, $context["task"], "status", [], "any", false, false, false, 202) == 2)) {
                // line 203
                echo "                                    <td class=\"text-left\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "task", [], "any", false, false, false, 203), "html", null, true);
                echo "</td>
                                    <td class=\"text-left\"> ";
                // line 204
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "contractor", [], "any", false, false, false, 204), "html", null, true);
                echo "</td>
                                    ";
                // line 205
                if ((twig_get_attribute($this->env, $this->source, $context["task"], "priority", [], "any", false, false, false, 205) == 1)) {
                    // line 206
                    echo "                                        <td class=\"pl-3\"><p>Low <i class=\"material-icons priority-pointer-green\">brightness_1</i>
                                            </p></td>
                                    ";
                } elseif ((twig_get_attribute($this->env, $this->source,                 // line 208
$context["task"], "priority", [], "any", false, false, false, 208) == 2)) {
                    // line 209
                    echo "                                        <td class=\"pl-3\"><p>Medium <i class=\"material-icons priority-pointer-yellow\">brightness_1</i>
                                            </p></td>
                                    ";
                } elseif ((twig_get_attribute($this->env, $this->source,                 // line 211
$context["task"], "priority", [], "any", false, false, false, 211) == 3)) {
                    // line 212
                    echo "                                        <td class=\"pl-3\"><p>High <i class=\"material-icons priority-pointer-red\">brightness_1</i>
                                            </p></td>
                                    ";
                }
                // line 215
                echo "                                    <td class=\"text-left\">";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "dueDate", [], "any", false, false, false, 215), "d-M-y"), "html", null, true);
                echo "</td>
                                    <td>
                                        <ul class=\"nav nav-pills float-right\">
                                            <li class=\"nav-item dropup dropleft\">
                                                <a class=\"nav-link material-icon\" style=\"padding:0px;,height:3px;\"
                                                   data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\"
                                                   aria-expanded=\"false\"><i class=\"material-icons \"
                                                                            style=\"color:black;\">more_horiz</i></a>
                                                <div class=\"dropdown-menu\" style=\"\">
                                                    <a class=\"dropdown-item\"
                                                       href=\"/main/projects/deleteTask/";
                // line 225
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "id", [], "any", false, false, false, 225), "html", null, true);
                echo "\">Delete</a>
                                                    <a class=\"dropdown-item\"
                                                       href=\"/main/projects/deleteTask/";
                // line 227
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "id", [], "any", false, false, false, 227), "html", null, true);
                echo "\">Update</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                ";
            }
            // line 233
            echo "                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['task'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 235
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Accordion card -->
        <div class=\"card\">

            <!-- Card header -->
            <div class=\"card-header\" role=\"tab\" id=\"headingWaiting\">
                <a data-toggle=\"collapse\" data-parent=\"#accordionEx\" href=\"#collapseOne1\" aria-expanded=\"false\"
                   aria-controls=\"collapseWaiting\">
                    <h5 class=\"mb-0\" style=\"font-size: 20px;color:rgb(68, 68, 68);\">Waiting</h5>
                </a>
            </div>

            <!-- Card body -->
            <div id=\"collapseOne1\" class=\"collapse show\" role=\"tabpanel\" aria-labelledby=\"headingWaiting\"
                 data-parent=\"#accordionEx\">
                <div class=\"card-body\">
                    <table id=\"Waiting\" class=\"table table-secondary workspace-table\">
                        <thead>
                        <tr>
                            <th scope=\"col\">Task</th>
                            <th scope=\"col\">Contractor</th>
                            <th scope=\"col\">Priority</th>
                            <th scope=\"col\">Due date</th>
                            <th scope=\"col\" class=\"text-right\">Options</th>
                        </tr>
                        </thead>
                        <tbody class=\"table-task\">
                        ";
        // line 266
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["tasks"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["task"]) {
            // line 267
            echo "                            <tr class=\"table-active\">
                                ";
            // line 268
            if ((twig_get_attribute($this->env, $this->source, $context["task"], "status", [], "any", false, false, false, 268) == 3)) {
                // line 269
                echo "                                    <td class=\"text-left\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "task", [], "any", false, false, false, 269), "html", null, true);
                echo "</td>
                                    <td class=\"text-left\"> ";
                // line 270
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "contractor", [], "any", false, false, false, 270), "html", null, true);
                echo "</td>
                                    ";
                // line 271
                if ((twig_get_attribute($this->env, $this->source, $context["task"], "priority", [], "any", false, false, false, 271) == 1)) {
                    // line 272
                    echo "                                        <td class=\"pl-3\"><p>Low <i class=\"material-icons priority-pointer-green\">brightness_1</i>
                                            </p></td>
                                    ";
                } elseif ((twig_get_attribute($this->env, $this->source,                 // line 274
$context["task"], "priority", [], "any", false, false, false, 274) == 2)) {
                    // line 275
                    echo "                                        <td class=\"pl-3\"><p>Medium <i class=\"material-icons priority-pointer-yellow\">brightness_1</i>
                                            </p></td>
                                    ";
                } elseif ((twig_get_attribute($this->env, $this->source,                 // line 277
$context["task"], "priority", [], "any", false, false, false, 277) == 3)) {
                    // line 278
                    echo "                                        <td class=\"pl-3\"><p>High <i class=\"material-icons priority-pointer-red\">brightness_1</i>
                                            </p></td>
                                    ";
                }
                // line 281
                echo "                                    <td class=\"text-left\">";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "dueDate", [], "any", false, false, false, 281), "d-M-y"), "html", null, true);
                echo "

                                    </td>
                                    <td>
                                        <ul class=\"nav nav-pills float-right\">
                                            <li class=\"nav-item dropup dropleft\">
                                                <a class=\"nav-link material-icon\" style=\"padding:0px;,height:3px;\"
                                                   data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\"
                                                   aria-expanded=\"false\"><i class=\"material-icons \"
                                                                            style=\"color:black;\">more_horiz</i></a>
                                                <div class=\"dropdown-menu\" style=\"\">
                                                    <a class=\"dropdown-item\"
                                                       href=\"/main/projects/deleteTask/";
                // line 293
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "id", [], "any", false, false, false, 293), "html", null, true);
                echo "\">Delete</a>
                                                    <a class=\"dropdown-item\"
                                                       href=\"/main/projects/deleteTask/";
                // line 295
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "id", [], "any", false, false, false, 295), "html", null, true);
                echo "\">Update</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                ";
            }
            // line 301
            echo "                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['task'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 303
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "projects/show_tasks.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  472 => 303,  465 => 301,  456 => 295,  451 => 293,  435 => 281,  430 => 278,  428 => 277,  424 => 275,  422 => 274,  418 => 272,  416 => 271,  412 => 270,  407 => 269,  405 => 268,  402 => 267,  398 => 266,  365 => 235,  358 => 233,  349 => 227,  344 => 225,  330 => 215,  325 => 212,  323 => 211,  319 => 209,  317 => 208,  313 => 206,  311 => 205,  307 => 204,  302 => 203,  300 => 202,  297 => 201,  293 => 200,  255 => 164,  248 => 162,  239 => 156,  234 => 154,  219 => 143,  212 => 138,  210 => 137,  206 => 135,  204 => 134,  200 => 132,  198 => 131,  194 => 130,  189 => 129,  187 => 128,  184 => 127,  180 => 126,  57 => 5,  53 => 4,  47 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "projects/show_tasks.html.twig", "/home/dev/mps/templates/projects/show_tasks.html.twig");
    }
}
