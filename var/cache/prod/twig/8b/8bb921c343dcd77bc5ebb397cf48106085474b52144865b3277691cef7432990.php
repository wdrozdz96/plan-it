<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* sign_in.html.twig */
class __TwigTemplate_38dbb8c12016cb8a8e5343dbd1ba234de99a5cccb8901dbd40c93e83131d718b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "sign_in.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Log in!";
    }

    // line 3
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <meta name=\"google-signin-client_id\" content=\"YOUR_CLIENT_ID.apps.googleusercontent.com\">
";
    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    <style>
        body {
            background: linear-gradient(to bottom, rgba(0, 0, 0, .3) 0, rgba(0, 0, 0, .5) 100%), url(\"https://images.unsplash.com/photo-1472289065668-ce650ac443d2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80\");
            background-size: cover;
            background-repeat: no-repeat;
        }

        .login-form-wrapper {
            height: 100vh;
            width: 100%;
            justify-content: center;
            align-items: center;
        }

        .login-form {
            width: 400px;
        }

        .abcRioButton {
            width: 100% !important;
        }

        .big-logo-wrapper {
            display: flex;
            width: 100%;
            justify-content: center;
        }

        .big-logo {
            height: 9vh;
            width: 9vh;
        }


    </style>


    <div class=\"row login-form-wrapper\">
        <form method=\"post\">
            ";
        // line 46
        if (($context["error"] ?? null)) {
            // line 47
            echo "                <div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, ($context["error"] ?? null), "messageKey", [], "any", false, false, false, 47), twig_get_attribute($this->env, $this->source, ($context["error"] ?? null), "messageData", [], "any", false, false, false, 47), "security"), "html", null, true);
            echo "</div>
            ";
        }
        // line 49
        echo "            <div class=\"card login-form\">
                <div class=\"card-body bg-light\">
                    <div class=\"big-logo-wrapper\">
                        <img class=\"text-center big-logo mb-4\"
                             src=\"https://www.freelogodesign.org/file/app/client/thumb/54307ce0-ac13-4793-8e4b-24f43579daa5_200x200.png?1574947640270\">
                    </div>

                    <h1 class=\"h3 mb-4 p-1 font-weight-normal text-center\">Sign in</h1>

                    <div class=\"form-group\">
                        <input type=\"email\" value=\"";
        // line 59
        echo twig_escape_filter($this->env, ($context["last_username"] ?? null), "html", null, true);
        echo "\" name=\"email\" id=\"inputEmail\"
                               class=\"form-control\" placeholder=\"Email\" required autofocus>
                    </div>
                    <div class=\"form-group\">
                        <input type=\"password\" name=\"password\" id=\"inputPassword\" class=\"form-control\"
                               placeholder=\"Password\" required>
                    </div>
                    <input type=\"hidden\" name=\"_csrf_token\"
                           value=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("authenticate"), "html", null, true);
        echo "\"
                    >
                    <button class=\"btn btn-primary btn-block mb-3 mt-4\" type=\"submit\">
                        Sign in
                    </button>
                    <div class=\"form-group \">
                        <div id=\"my-signin2\"></div>
                    </div>
                    <div class=\"form-group text-center mt-4\">
                        You dont have account?<br>
                        <a href=\"/welcome\">Sign up.</a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script>
        function onSuccess(googleUser) {
            console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
        }

        function onFailure(error) {
            console.log(error);
        }

        function renderButton() {
            gapi.signin2.render('my-signin2', {
                'scope': 'profile email',
                'width': 240,
                'height': 50,
                'longtitle': true,
                'theme': 'dark',
                'onsuccess': onSuccess,
                'onfailure': onFailure
            });
        }
    </script>

    <script src=\"https://apis.google.com/js/platform.js?onload=renderButton\" async defer></script>
";
    }

    public function getTemplateName()
    {
        return "sign_in.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 67,  129 => 59,  117 => 49,  111 => 47,  109 => 46,  68 => 7,  64 => 6,  59 => 4,  55 => 3,  48 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "sign_in.html.twig", "/home/dev/mps/templates/sign_in.html.twig");
    }
}
