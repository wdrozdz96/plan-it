<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* projects/project_form.html.twig */
class __TwigTemplate_f41d0a2c82d570d648fa607392b0e04c61bcbae8f2c155b2757dd769c4ff5660 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "projects/project_form.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Register";
    }

    // line 3
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    <style>
        .horizontal_navbar_wrapper {
            height: auto;
            width: 13%;
            justify-content: center;
            box-shadow: 0px 0px 2px 0px rgba(156, 156, 156, 1);
            position: relative;
            text-align: center;
        }

        .project-add-trigger {
            width: 100%;
        }

        .strap {
            height: 10px;
            width: 100%;
            margin-top: 0;
        }

        .small-strap {
            height: 3px;
            width: 35%;
        }

        .bottom-content-wrapper {
            position: absolute;
            bottom: 3vh;
            display: flex;
            justify-content: center;
            width: 100%;
        }

        .layout {
            display: flex;
        }

        .task-add-trigger {
            width: 150px;
        }

        .content-wrapper {
            width: 100%;
            display: flex;
            justify-content: center;
            padding: 90px;
            padding-top: 0px;
            min-height: 100vh;

        }

        .content {
            padding-top: 160px;
            width: 100%;
        }

        .not-selected-project {
            width: 30%;
            display: flex;
            justify-content: center;
        }

        .projectheader {
            width: 85%;
            height: 140px;
            position: absolute;
            box-shadow: 0 3px 3px 0px #c3c3c3;

        }

        .project-name {
            margin-top: 1px;
            margin-left: 56px;
            font-size: 47px;
        }

        .project-name-label {
            margin-top: 25px;
            margin-left: 63px;
            margin-bottom: 1px;
        }

        .data-form {
            width: 300px;
        }

    </style>
    <div class=\"layout\">

        <div class=\"horizontal_navbar_wrapper\">
            <span class=\"badge badge-secondary mt-0 strap\"> </span>
            <p class=\"text-muted mt-2 mb-1\">Workspace</p>
            <h3 class=\"mb-0\">";
        // line 99
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 99), "get", [0 => "currentWorkspaceName"], "method", false, false, false, 99), "html", null, true);
        echo "</h3>
            <a class=\"\" href=\"/main\">BACK</a><br>
            <span class=\"badge badge-secondary  small-strap\"> </span>
            <p class=\"text-muted mb-1 mt-1\">User</p>
            <h4 class=\"mt-0\">";
        // line 103
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "User", [], "any", false, false, false, 103), "name", [], "any", false, false, false, 103), "html", null, true);
        echo "</h4>

            <!-- add-project-modal trigger -->
            <div class=\"text-center\">
                <button type=\"button\" class=\"btn btn-primary project-add-trigger mt-4 mb-3\" data-toggle=\"modal\"
                        data-target=\"#addModal\">CREATE PROJECT
                </button>
                <p class=\"text-muted\">Projects</p>
                <span class=\"badge badge-secondary mb-3 small-strap\">  </span>
                ";
        // line 112
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment(Symfony\Bridge\Twig\Extension\HttpKernelExtension::controller("App\\Controller\\ProjectController::showProjects"));
        echo "
            </div>

            <!-- add-project-modal -->
            <div class=\"modal fade\" id=\"addModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"
                 aria-hidden=\"true\">
                <div class=\"modal-dialog\" role=\"document\">
                    <div class=\"modal-content\">
                        <div class=\"modal-header\">
                            <h5 class=\"modal-title\" id=\"exampleModalLabel\">Create project</h5>
                        </div>
                        <div class=\"modal-body\">
                            ";
        // line 124
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["project_form"] ?? null), 'form_start');
        echo "
                            ";
        // line 125
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["project_form"] ?? null), "project", [], "any", false, false, false, 125), 'row');
        echo "
                        </div>
                        <div class=\"modal-footer\">
                            <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                            ";
        // line 129
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["project_form"] ?? null), "create", [], "any", false, false, false, 129), 'widget', ["label" => "Create", "attr" => ["class" => "btn btn-primary"]]);
        echo "
                            ";
        // line 130
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["project_form"] ?? null), 'form_end');
        echo "

                        </div>
                    </div>
                </div>
            </div>


            <button type=\"button\" class=\"btn btn-secondary project-add-trigger mt-4 mb-3\" data-toggle=\"modal\"
                    data-target=\"#addUserModal\">add member
            </button>
        </div>

        <div class=\"modal fade\" id=\"addUserModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"
             aria-hidden=\"true\">
            <div class=\"modal-dialog\" role=\"document\">
                <div class=\"modal-content\">

                    <div class=\"modal-header\">
                        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Add member</h5>
                    </div>
                    <div class=\"modal-body\">
                        ";
        // line 152
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["user_form"] ?? null), 'form_start');
        echo "
                        <p>Keep in mind that the member must be signed up.</p>
                        ";
        // line 154
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["user_form"] ?? null), "users", [], "any", false, false, false, 154), 'row');
        echo "
                    </div>
                    <div class=\"modal-footer\">
                        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                        ";
        // line 158
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["user_form"] ?? null), "addUser", [], "any", false, false, false, 158), 'widget', ["label" => "add member", "attr" => ["class" => "btn btn-primary"]]);
        echo "
                        ";
        // line 159
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["user_form"] ?? null), 'form_end');
        echo "

                    </div>
                </div>
            </div>
        </div>
        ";
        // line 165
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 165), "get", [0 => "currentProject"], "method", false, false, false, 165)) {
            // line 166
            echo "
            <div class=\"content-wrapper\">
                <div class=\"projectheader\">
                    <p class=\"text-muted project-name-label\">Project</p>
                    <h1 class=\"project-name\">";
            // line 170
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 170), "get", [0 => "currentProjectName"], "method", false, false, false, 170), "html", null, true);
            echo " </h1>
                    <div style=\"padding-left: 90%\">
                        <a data-toggle=\"modal\" class=\"btn btn-danger btn-sm ml-2\" style=\"color:white\"
                           data-target=\"#removeProjectModal\">Delete project</a>
                    </div>


                    <!-- remove-modal -->
                    <div class=\"modal fade\" id=\"removeProjectModal\" tabindex=\"-1\" role=\"dialog\"
                         aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
                        <div class=\"modal-dialog\" role=\"document\">
                            <div class=\"modal-content\">
                                <div class=\"modal-header\">
                                    <h5 class=\"modal-title\" id=\"exampleModalLabel\">Are you sure?</h5>
                                </div>
                                <div class=\"modal-body\">
                                    <a class=\"blockquote mt-3 mb-3\">This action will remove project and all tasks that
                                        it contains</a>
                                </div>
                                <div class=\"modal-footer\">
                                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                                    <a class=\"btn btn-primary\"
                                       href=\"projects/deleteProject/";
            // line 192
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 192), "get", [0 => "currentProject"], "method", false, false, false, 192), "html", null, true);
            echo "\">DELETE</a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class=\"content\">

                    <div class=\"\">
                        <button type=\"button\" class=\"btn btn-primary task-add-trigger mt-4 mb-3\" data-toggle=\"modal\"
                                data-target=\"#addTaskModal\">CREATE TASK
                        </button>
                    </div>


                    <div class=\"modal fade\" id=\"addTaskModal\" tabindex=\"-1\" role=\"dialog\"
                         aria-labelledby=\"exampleModalLabel\"
                         aria-hidden=\"true\">
                        <div class=\"modal-dialog\" role=\"document\">
                            <div class=\"modal-content\">
                                <div class=\"modal-header\">
                                    <h5 class=\"modal-title\" id=\"exampleModalLabel\">Add new task</h5>
                                </div>
                                <div class=\"modal-body\">
                                    ";
            // line 218
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["task_form"] ?? null), 'form_start');
            echo "
                                    ";
            // line 219
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["task_form"] ?? null), "task", [], "any", false, false, false, 219), 'row');
            echo "
                                    ";
            // line 220
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["task_form"] ?? null), "description", [], "any", false, false, false, 220), 'row');
            echo "
                                    ";
            // line 221
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["task_form"] ?? null), "contractor", [], "any", false, false, false, 221), 'row');
            echo "

                                    ";
            // line 223
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["task_form"] ?? null), "priority", [], "any", false, false, false, 223), 'row');
            echo "
                                    ";
            // line 224
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["task_form"] ?? null), "status", [], "any", false, false, false, 224), 'row');
            echo "
                                    <div class=\"data-form\">";
            // line 225
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["task_form"] ?? null), "dueDate", [], "any", false, false, false, 225), 'row');
            echo "</div>

                                </div>
                                <div class=\"modal-footer\">
                                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                                    ";
            // line 230
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["task_form"] ?? null), "addTask", [], "any", false, false, false, 230), 'widget', ["label" => "create task", "attr" => ["class" => "btn btn-primary"]]);
            echo "
                                    ";
            // line 231
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["task_form"] ?? null), 'form_end');
            echo "

                                </div>

                            </div>
                        </div>
                    </div>
                    ";
            // line 238
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment(Symfony\Bridge\Twig\Extension\HttpKernelExtension::controller("App\\Controller\\ProjectController::showTasks"));
            echo "
                </div>
            </div>
        ";
        } else {
            // line 242
            echo "            <div class=\"content-wrapper\">
                <div class=\"content\">
                    <div class=\"not-selected-project\">
                        <h1>Select or create project</h1>
                        <div class=\"text-center\">

                        </div>
                    </div>
                </div>
            </div>
        ";
        }
        // line 253
        echo "    </div>

";
    }

    public function getTemplateName()
    {
        return "projects/project_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  388 => 253,  375 => 242,  368 => 238,  358 => 231,  354 => 230,  346 => 225,  342 => 224,  338 => 223,  333 => 221,  329 => 220,  325 => 219,  321 => 218,  292 => 192,  267 => 170,  261 => 166,  259 => 165,  250 => 159,  246 => 158,  239 => 154,  234 => 152,  209 => 130,  205 => 129,  198 => 125,  194 => 124,  179 => 112,  167 => 103,  160 => 99,  65 => 6,  61 => 5,  55 => 3,  48 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "projects/project_form.html.twig", "/home/dev/mps/templates/projects/project_form.html.twig");
    }
}
