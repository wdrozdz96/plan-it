<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/connect/google' => [[['_route' => 'connect_google_start', '_controller' => 'App\\Controller\\GoogleController::connectAction'], null, null, null, false, false, null]],
        '/connect/google/check' => [[['_route' => 'connect_google_check', '_controller' => 'App\\Controller\\GoogleController::connectCheckAction'], null, null, null, false, false, null]],
        '/main/projects' => [
            [['_route' => 'app_addUsers', '_controller' => 'App\\Controller\\ProjectController::new'], null, null, null, false, false, null],
            [['_route' => 'app_displayProjects', '_controller' => 'App\\Controller\\ProjectController::showProjects'], null, null, null, false, false, null],
            [['_route' => 'app_displayTasks', '_controller' => 'App\\Controller\\ProjectController::showTasks'], null, null, null, false, false, null],
            [['_route' => 'projects', '_controller' => 'App\\Controller\\ProjectController::displayProjects'], null, null, null, false, false, null],
        ],
        '/welcome' => [[['_route' => 'app_welcome', '_controller' => 'App\\Controller\\RegistrationController::register'], null, null, null, false, false, null]],
        '/sign-in' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/main' => [
            [['_route' => 'app_addWorkspace', '_controller' => 'App\\Controller\\WorkspaceController::new'], null, null, null, false, false, null],
            [['_route' => 'app_main', '_controller' => 'App\\Controller\\WorkspaceController::showAction'], null, null, null, false, false, null],
        ],
        '/' => [[['_route' => 'index', '_controller' => 'App\\Controller\\WorkspaceController::new'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/projects/select/([^/]++)/([^/]++)(*:41)'
                .'|/main/(?'
                    .'|projects/delete(?'
                        .'|Task/([^/]++)(*:88)'
                        .'|Project/([^/]++)(*:111)'
                    .')'
                    .'|delete/([^/]++)(*:135)'
                    .'|select/([^/]++)/([^/]++)(*:167)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        41 => [[['_route' => 'app_project_select', '_controller' => 'App\\Controller\\ProjectController::select'], ['currentProject', 'currentProjectName'], null, null, false, true, null]],
        88 => [[['_route' => 'app_project_deletetask', '_controller' => 'App\\Controller\\ProjectController::deleteTask'], ['id'], null, null, false, true, null]],
        111 => [[['_route' => 'app_project_deleteproject', '_controller' => 'App\\Controller\\ProjectController::deleteProject'], ['id'], null, null, false, true, null]],
        135 => [[['_route' => 'app_workspace_delete', '_controller' => 'App\\Controller\\WorkspaceController::delete'], ['id'], null, null, false, true, null]],
        167 => [
            [['_route' => 'app_workspace_select', '_controller' => 'App\\Controller\\WorkspaceController::select'], ['currentWorkspace', 'currentWorkspaceName'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
